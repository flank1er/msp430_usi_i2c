#include "msp430g2452.h"
#include "sys/types.h"
#include "uart_sw.h"

uint8_t uart_send_char(uint8_t ch){
    int i;
    ch=~ch;
    TACCR0=98;   //9600
    TACCTL0=CCIE; // enable interrupt
	TACTL |=MC_1; // enable timer
    __bis_SR_register(LPM0_bits + GIE); // goto sleep
    // START bit
    uOUT &= ~TXD;
    for(i=0;i<8;i++) // DATA transmission
    {
        __bis_SR_register(LPM0_bits + GIE); // goto sleep
        uOUT=(ch&(1<<i)) ? uOUT & ~TXD: uOUT | TXD;
    }
    // STOP bit
    __bis_SR_register(LPM0_bits + GIE); // goto sleep
    P1OUT &= ~TXD;
    __bis_SR_register(LPM0_bits + GIE); // goto sleep
    P1OUT |=TXD;

    TACCTL0=0; // disable interrupt
	TACTL &= ~MC_1; // disable timer;
    return ch;
}

void uart_send_string(char* str) {
   while (*str) {
        uart_send_char(*str++);
   }
}

void uart_send_hex_uint8(uint8_t num) {
    static const uint8_t symbol[16] ="0123456789ABCDEF";
    uart_send_char('0');
    uart_send_char('x');
    uart_send_char(symbol[(num >> 4)]);
    uart_send_char(symbol[(num & 0x0f)]);
}

void uart_send_uint8(uint8_t num){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
            uart_send_char(sym[i]);
        j++;
    }
}

