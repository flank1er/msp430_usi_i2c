// http://www.count-zero.ru/2017/msp430_usi_i2c/#2
#include <msp430g2452.h>
#include <sys/types.h>
#include "timer_a.h"
#include "uart_sw.h"

#define LED BIT0

int main (void)
{
	// watchdog setup
    WDTCTL=WDTPW | WDTHOLD; //turn off watchdog
	// GPIO setup
    P1DIR = LED; // set P1.0 as Push Pull mode(PP)
    P1OUT &=~LED;

   	// F_CPU 1MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL  = CALDCO_1MHZ;
    BCSCTL2 &=~(DIVS_0); //SMCLK=DCO
    BCSCTL3 |= LFXT1S_2; //ACLK= VLO =~ 12KHz
	// TimerA init
    TACTL=TASSEL_2 + ID_0 + TACTL;
    /*  TASSEL_2  =use SMCLK;
        MC_1      =continous to TACCR0;
        ID_0      =prescaler  = 1;
        TACLR     =clean counter TAR;
        TAIE;     =enable timer interrupt;
    */
	// software uart
    uDIR += TXD;
	//interrupt
    __enable_interrupt();
	// main loop
	uint8_t i=0;
    for(;;){
		wait_ms(1000);
	    P1OUT |= LED;
		uart_send_string("i= ");
		uart_send_uint8(i++);
     	uart_send_char('\n');
	    P1OUT &= ~LED;
	};

    return 0;
 }
