#ifndef __UART_SW_H__
#define __UART_SW_H__

#define TXD   BIT1
#define RXD   BIT2
#define uOUT  P1OUT
#define uDIR  P1DIR

extern uint8_t uart_send_char(uint8_t ch);
extern void uart_send_uint8(uint8_t num);
extern void uart_send_string(char* str);
extern void uart_send_hex_uint8(uint8_t num);
#endif
