#ifndef __I2C_SW_H__
#define __I2C_SW_H__

int start_i2c();
int stop_i2c();
uint8_t send_i2c(uint8_t value);
uint8_t read_i2c(int last);
uint8_t i2c_begin(uint8_t adr);
uint8_t i2c_request_from(uint8_t ard, uint8_t cnt);
uint8_t i2c_read();
#endif
