// http://www.count-zero.ru/2017/msp430_usi_i2c/#3
#include <msp430g2452.h>
#include <sys/types.h>
#include "timer_a.h"
#include "uart_sw.h"
#include "i2c_sw.h"
#include "ds3231.h"

#define LED BIT0

int main (void)
{
    //count=0;
	// watchdog setup
    WDTCTL=WDTPW | WDTHOLD; //turn off watchdog
	// GPIO setup
    P1DIR = LED; // set P1.0 as Push Pull mode(PP)
    P1OUT &=~LED;

   	// F_CPU 1MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL  = CALDCO_1MHZ;
    BCSCTL2 &=~(DIVS_0); //SMCLK=DCO
    BCSCTL3 |= LFXT1S_2; //ACLK= VLO =~ 12KHz
	// TimerA init
    TACTL=TASSEL_2 + ID_0 + TACTL;
    /*  TASSEL_2  =use SMCLK;
        MC_1      =continous to TACCR0;
        ID_0      =prescaler  = 1;
        TACLR     =clean counter TAR;
        TAIE;     =enable timer interrupt;
    */
	// software uart
    uDIR += TXD;
    // rtc init
    ds3231_init();
	//interrupt
    __enable_interrupt();
	// RTC get data
	if (ds3231_update() && ds3231_update_status_control())
    {
        //ds3231_reset_control(DS3231_INTCH);
        ds3231_set_register(DS3231_CONTROL_ADDRESS, 0x0);
        ds3231_set_register(DS3231_STATUS_ADDRESS, 0x0);
        //ds3231_update_status_control();
    }
    uint8_t i=0;
    for(;;){
        wait_ms(10000); 
        P1OUT |= LED;
                          
        ds3231_print_calendar();
        if (ds3231_is_alarm(ALARM_A2))
            uart_send_string("Alarm 2 is ON ");
        else
            uart_send_string("Alarm 2 is OFF ");

        if (ds3231_is_alarm(ALARM_A1))
            uart_send_string("Alarm 1 is ON\n");
        else
            uart_send_string("Alarm 1 is OFF\n");
        
        // set timer ALARM A1 on two minutes
        if (++i == 5) {        
            uart_send_string("\nSet alarm A1...\n");
            ds3231_set_alarm_a1(A1_MATCH_MINUTES,ds3231_get(SECOND)%60, (ds3231_get(MINUTE)+2)%60, 0,0);
            
            uart_send_string("ctl= ");
            uart_send_hex_uint8(ds3231_get_control());
            uart_send_string(" sts= ");
            uart_send_hex_uint8(ds3231_get_status());
            uart_send_char('\n');

            ds3231_print_alarm_1();
            __bis_SR_register(LPM4_bits + GIE);  
            wait_ms(10);
            uart_send_string("\nWAKEUP!\n");
            ds3231_reset_control(DS3231_A1IE|DS3231_INTCH);   
            ds3231_update();     
            ds3231_print_calendar();
        }

        P1OUT&=~LED;
	};

    return 0;
 }

 
