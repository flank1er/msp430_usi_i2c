#include "msp430g2452.h"
#include "sys/types.h"
#include "i2c_sw.h"

#define SCL     BIT6
#define SDA     BIT7
#define sclOUT  P1OUT
#define sdaOUT  P1OUT
#define sclDIR  P1DIR
#define sdaDIR  P1DIR
#define sclREN  P1REN
#define sdaREN  P1REN
#define sdaIN   P1IN
#define sclIN   P1IN

#define QDEL  __delay_cycles(20);
#define HDEL  __delay_cycles(40);

#define SDA_I2C_HI    sdaDIR&=~SDA
#define SCL_I2C_HI    sclDIR&=~SCL

#define SDA_I2C_LO    sdaDIR|=SDA
#define SCL_I2C_LO    sclDIR|=SCL

#define SCL_TOGGLE_I2C    HDEL; SCL_I2C_HI;HDEL;SCL_I2C_LO;

#define initLEN    2
#define rtcLEN     7

#define LAST 1
#define NOLAST 0

static uint8_t i2c_count;

/////////////// I2C //////////////////////////////////
int start_i2c() {
   int ret=64;
   uint8_t valueSDA, valueSCL;
   // init I2C
   sdaOUT &=~SDA; sclOUT &=~SCL;
   do {
      SDA_I2C_HI;
      SCL_I2C_HI;
      QDEL;

      valueSDA=sdaIN & SDA;
      valueSCL=sclIN & SCL;
   } while((!valueSDA || !valueSCL) && --ret>0);

   if (!ret) return 0;

   SDA_I2C_LO;QDEL;
   SCL_I2C_LO;QDEL;

   return ret;
}

int stop_i2c(){
   SDA_I2C_LO; SCL_I2C_LO;HDEL;
   SCL_I2C_HI;QDEL;
   SDA_I2C_HI;QDEL;
   if (!(sdaIN & SDA) || !(sclIN & SCL))
      return 1;
   else
      return 0;
}

uint8_t send_i2c(uint8_t value) {
   int bits=8;
   while(bits>0)
   {
      bits--;
      SCL_I2C_LO; QDEL;
      while((sclIN & SCL));
      if (value & (1<<bits))
     SDA_I2C_HI;
      else
     SDA_I2C_LO;
      SCL_TOGGLE_I2C;
   }

   //get  ACK
   QDEL; SDA_I2C_HI;
   QDEL; SCL_I2C_HI;
   while(!(sclIN & SCL));

   QDEL; uint8_t ack;
   ack=(sdaIN & SDA);
   QDEL;

   SCL_I2C_LO;
   HDEL;

   return ack;
}

uint8_t read_i2c(int last) {
   uint8_t c, b;
   int i; b=0;
   SDA_I2C_HI;
   for(i=0;i<8;i++)
   {
      HDEL;
      SCL_I2C_HI;
      c=(sdaIN & SDA);
      b <<=1;
      if(c) b|=1;
      HDEL;
      SCL_I2C_LO;
   }

   if(last)
      SDA_I2C_HI;
   else
      SDA_I2C_LO;

   SCL_TOGGLE_I2C;
   SDA_I2C_HI;

   return b;
}

uint8_t i2c_begin(uint8_t adr) {
	//////////// WRITE ADDRESS /////////////////////
	// START bit
	int attempts;
	uint8_t ack=0xff;
    attempts=start_i2c();

    if (attempts)
		ack=send_i2c(adr);
	// 0 - Error, 1 - Success
	return (!attempts || ack ) ? 0: 1;
}

uint8_t i2c_request_from(uint8_t ard, uint8_t cnt) {
	i2c_count=cnt;
	return (i2c_begin((ard<<1)|1)) ? 1 : 0;
}

uint8_t i2c_read() {
	uint8_t ret=0;
	if (i2c_count)
	{
		i2c_count--;
		if  (!i2c_count) {
			ret=read_i2c(LAST);
			stop_i2c();
		} else
			ret=read_i2c(NOLAST);
	}
	return ret;
}
