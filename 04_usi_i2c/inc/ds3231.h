#ifndef __DS3231_H__
#define __DS3231_H__

#define DS1307_ADR    0x68
#define DS1307_CALENDAR 0x00
#define DS1307_CONTROL_ADDRESS 0x07

#define DS3231_CORRECTION 0

#define DS3231_ALARM1_ADDRESS       0x07
#define DS3231_ALARM2_ADDRESS       0x0B
#define DS3231_CONTROL_ADDRESS      0x0E
#define DS3231_STATUS_ADDRESS       0x0F
#define DS3231_AGING_ADDRESS        0x10
#define DS3231_TEMPERATURE_ADDRESS  0x11

// control register
#define DS3231_A1IE  0x01
#define DS3231_A2IE  0x02
#define DS3231_INTCH 0x04
#define DS3231_CONV  0x20
#define DS3231_BBSQW 0x40
#define DS3231_EOSC	 0x80

#define DS3231_1HZ 0x0
#define DS3231_1024HZ 0x08
#define DS3231_4096HZ 0x10
#define DS3231_8192HZ 0x18
#define DS3231_32768HZ 0x01

// status register
#define DS3231_A1F	0x01
#define DS3231_A2F	0x02
#define DS3231_BSY	0x04
#define DS3231_32KHZ 0x08
#define DS3231_OSF	0x80

typedef enum FIELD {
	YEAR=6,
	MONTH=5,
	DATE=4,
	DAY=3,
	HOUR=2,
	MINUTE=1,
	SECOND=0
} FIELD;

//Alarm masks
typedef enum ALARM_A1_TYPE {
    A1_EVERY_SECOND		= 0x0F,
    A1_MATCH_SECONDS	= 0x0E,
    A1_MATCH_MINUTES	= 0x0C,     //match minutes *and* seconds
    A1_MATCH_HOURS		= 0x08,     //match hours *and* minutes, seconds
    A1_MATCH_DATE		= 0x00,     //match date *and* hours, minutes, seconds
    A1_MATCH_DAY		= 0x10,     //match day *and* hours, minutes, seconds
} ALARM_A1_TYPE;

typedef enum ALARM_A2_TYPE {
    A2_EVERY_MINUTE		= 0x8E,
    A2_MATCH_MINUTES	= 0x8C,     //match minutes
    A2_MATCH_HOURS		= 0x88,     //match hours *and* minutes
    A2_MATCH_DATE		= 0x80,     //match date *and* hours, minutes
    A2_MATCH_DAY		= 0x90,     //match day *and* hours, minutes
} ALARM_A2_TYPE;

typedef enum ALARM_NUMBER {
    ALARM_A1=0x01, 
    ALARM_A2=0x02
} ALARM_NUMBER ;


extern void ds3231_init();
extern uint8_t ds3231_set_address(uint8_t adr);
extern uint8_t ds3231_set_register(uint8_t adr, uint8_t value);
extern uint8_t ds3231_update();
extern uint8_t ds3231_get(FIELD value);
extern uint8_t ds3231_update_status_control();
extern char ds3231_get_temperature();
extern char ds3231_get_temperature_fraction();
// CONTROL REGISTER //////////////////////////
extern uint8_t ds3231_get_control();
extern void ds3231_preset_control(uint8_t value);
extern void ds3231_set_control(uint8_t value);
extern void ds3231_reset_control(uint8_t value);
// STATUS REGISTER /////////////////////////
extern uint8_t ds3231_get_status();
extern void ds3231_reset_status(uint8_t status);
extern void ds3231_set_status(uint8_t status);
// ALARM ///////////////////////
extern uint8_t ds3231_is_alarm(ALARM_NUMBER num);
void ds3231_disable_alarm(ALARM_NUMBER num);
extern void ds3231_set_alarm_a1(ALARM_A1_TYPE type, uint8_t sec, uint8_t min, uint8_t hour ,uint8_t day);
extern void ds3231_set_alarm_a2(ALARM_A2_TYPE type, uint8_t min, uint8_t hour ,uint8_t day);
///////////////////////////////////////////////
extern void ds3231_print_calendar();
extern void ds3231_print_alarm_1();
extern void ds3231_print_alarm_2();
#endif
