/* Based on https://github.com/flank1er/DS3231SQW/ */
#include "msp430g2452.h"
#include "sys/types.h"
#include "i2c_usi.h"
#include "ds3231.h"
#include "uart_sw.h"

#define FAILURE 0x0
#define SUCCESS 0x1
#define SQW		BIT3

volatile uint32_t sec;
static uint32_t prev_sec;
static uint8_t cal[DS1307_CONTROL_ADDRESS];

static char tempMSB;
static uint8_t tempLSB;
static void ds3231_add_day();
// 10 bytes
typedef struct ALARM_t {
	unsigned second_a1 	:7;
    unsigned a1m1 		:1;
    unsigned minute_a1 	:7;
    unsigned a1m2 		:1;
    unsigned hour_a1 	:7;
    unsigned a1m3 		:1;
    unsigned day_date_a1 :6;
	unsigned dydt_a1	:1;
    unsigned a1m4 		:1;

    unsigned minute_a2 	:7;
    unsigned a2m2 		:1;
    unsigned hour_a2 	:7;
    unsigned a2m3 		:1;
    unsigned day_date_a2 :6;
    unsigned dydt_a2	:1;
    unsigned a2m4 		:1;

	unsigned control 	:8;
	unsigned status 	:8;
	unsigned aging 		:8;
} ALARM, *ALARM_t;

ALARM alarm;

#pragma vector=PORT2_VECTOR
__interrupt void Port2(void)
{
	if(P2IFG & SQW)
	{
		if (++sec >= 86400)
			ds3231_add_day();	
        
        if (ds3231_get_control() & DS3231_INTCH) 
        {                                
           __bic_SR_register_on_exit(LPM4_bits);
        }

		P2IFG&=~SQW;
	}
}

uint8_t dec_bcd(uint8_t dec) { return (dec/10*16) + (dec%10);};
uint8_t bcd_dec(uint8_t bcd) { return (bcd-6*(bcd>>4));};

void ds3231_init(){
	P2DIR &=~SQW; // HiZ mode
   	P2IES |= SQW; // falling front
   	P2IFG &=~SQW; // interrupt flag clearing
   	P2IE  |= SQW; // enable external interrupt
	sec=0;
	prev_sec=0;
}

static void ds3231_add_day() {
	cal[DAY]=cal[DAY]%7+1;

    uint8_t days;
    switch(cal[MONTH]) {
    case 4:
    case 6:
    case 9:
    case 11:
        days=30; break;
    case 2:
        days=(cal[YEAR] % 4 == 0) ? 29 :28; break;
    default: days=31;
    }

	if (cal[DATE] == days) {
		cal[DATE]=1;
		cal[MONTH]=cal[MONTH]+1;
	} else
		cal[DATE]=cal[DATE]+1;

	// new year
	if (cal[MONTH] > 12) {
		cal[MONTH] = 1;
		cal[YEAR] = cal[YEAR] + 1;
	}	
	sec=0;
}

uint8_t ds3231_set_address(uint8_t adr) {
	uint8_t ack=0xff;
	if(i2c_begin(DS1307_ADR<<1))
		ack=send_i2c(adr);
	stop_i2c();
    // 0 - Error, 1 - Success
	return (!ack) ? SUCCESS : FAILURE;
}

uint8_t ds3231_set_register(uint8_t adr, uint8_t value) {
    uint8_t ack=0xff;
    if(i2c_begin(DS1307_ADR<<1) && !send_i2c(adr))
        ack=send_i2c(value);
    stop_i2c();
    // 0 - Error, 1 - Success
    return (!ack) ? SUCCESS : FAILURE;
}

uint8_t ds3231_update() {
	uint8_t ret,i;
	if (ds3231_set_address(0x00) && i2c_request_from(DS1307_ADR,DS1307_CONTROL_ADDRESS))
    {
        for(i=0;i<DS1307_CONTROL_ADDRESS;i++)
		{
			if (i==0)
				cal[i]=bcd_dec(i2c_read() & 0x7f);
			else if (i==2)
				cal[i]=bcd_dec(i2c_read() & 0x3f);
			else
				cal[i]=bcd_dec(i2c_read());
		}
        ret=SUCCESS;
		sec=cal[SECOND] + cal[MINUTE]*60;
		for(i=0;i<cal[HOUR];i++) sec+=3600;
		sec+=DS3231_CORRECTION;
		prev_sec=sec;
     } else
     	ret=FAILURE;

	return ret;
}

uint8_t ds3231_get(FIELD value) {
	if (sec != prev_sec)
	{
		cal[SECOND]=sec%60;
		cal[MINUTE]=(sec%3600)/60;
		cal[HOUR]=sec/3600;
		prev_sec=sec;
	}
	return cal[value];
}

uint8_t ds3231_update_status_control() {
	uint8_t ret=FAILURE;
    if (ds3231_set_address(DS1307_CONTROL_ADDRESS) && i2c_request_from(DS1307_ADR,10))
    {
		char Buffer[10];
		char * ptr= (char*)Buffer;
		uint8_t i;
		for(i=0;i<10;i++)
			Buffer[i]=i2c_read();
		alarm=*(ALARM*)ptr;
		ret=SUCCESS;
	}
	return ret;
}

char ds3231_get_temperature() {
    uint8_t ret=0xff;
    if (ds3231_set_address(DS3231_TEMPERATURE_ADDRESS) && i2c_request_from(DS1307_ADR,2))
    {
    	tempMSB=i2c_read();
        tempLSB=i2c_read()>>6;
        ret=tempMSB;
    }
    return ret;
}

char ds3231_get_temperature_fraction() {
	char ret='?';
	switch (tempLSB) {
	case 0:
		ret='0'; break;
	case 1:
		ret='2'; break;
	case 2:
		ret='5'; break;
	case 3:
		ret='7';
	}

	return ret;
}
// STATUS REGISTER ///////////////////
uint8_t ds3231_get_status() {
    return (uint8_t)alarm.status;
}

void ds3231_set_status(uint8_t status) {
	status = alarm.status | status;
    ds3231_set_register(DS3231_STATUS_ADDRESS, status);
	ds3231_update_status_control();
}

void ds3231_reset_status(uint8_t status) {
    status = alarm.status & ~status;
    ds3231_set_register(DS3231_STATUS_ADDRESS, status);
    ds3231_update_status_control();
}

// CONTROL REGISTER //////////////////
uint8_t ds3231_get_control() {
    return (uint8_t)alarm.control;
}

void ds3231_preset_control(uint8_t value) {
    ds3231_set_register(DS3231_CONTROL_ADDRESS, value);
    ds3231_update_status_control();
}

void ds3231_set_control(uint8_t value) {
    value = value | alarm.control;
    ds3231_set_register(DS3231_CONTROL_ADDRESS, value);
    ds3231_update_status_control();
}

void ds3231_reset_control(uint8_t value) {
    value = alarm.control & ~value;
    ds3231_set_register(DS3231_CONTROL_ADDRESS, value);
    ds3231_update_status_control();
}

uint8_t ds3231_is_alarm(ALARM_NUMBER num){
	uint8_t ret=FAILURE;
	if (alarm.status & num)
	{
		ds3231_reset_status(num);
		ret=SUCCESS;
	}
	return ret;
}

void ds3231_disable_alarm(ALARM_NUMBER num) {
	if (num == ALARM_A1)
		ds3231_reset_control(DS3231_A1IE);
	else
		ds3231_reset_control(DS3231_A2IE);
}

void ds3231_set_alarm_a1(ALARM_A1_TYPE type, uint8_t sec, uint8_t min, uint8_t hour ,uint8_t day) {
    //// W R I T E /////////////////////////////////////
    ds3231_set_control(DS3231_A1IE|DS3231_INTCH);

    if(i2c_begin(DS1307_ADR<<1) &&  !send_i2c(DS3231_ALARM1_ADDRESS))
    {
    	send_i2c(dec_bcd(sec)|((type<<7) & 0x80));
	    send_i2c(dec_bcd(min)|((type<<6) & 0x80));
	    send_i2c(dec_bcd(hour)|((type<<5) & 0x80));
	    send_i2c(dec_bcd(day)|((type<<4) & 0x80)|((type<<2) & 0x40));
    }
    stop_i2c();
    // read for check ///////////////////////////////////
    ds3231_update_status_control();
}

void ds3231_set_alarm_a2(ALARM_A2_TYPE type, uint8_t min, uint8_t hour ,uint8_t day) {
    //// W R I T E /////////////////////////////////////
    ds3231_set_control(DS3231_A2IE|DS3231_INTCH);
    
    if(i2c_begin(DS1307_ADR<<1) &&  !send_i2c(DS3231_ALARM2_ADDRESS))
    {
	    send_i2c(dec_bcd(min)|((type<<6) & 0x80));
	    send_i2c(dec_bcd(hour)|((type<<5) & 0x80));
	    send_i2c(dec_bcd(day)|((type<<4) & 0x80)|((type<<2) & 0x40));
    }
    stop_i2c();    
    // read for check ///////////////////////////////////
    ds3231_update_status_control();
}


void ds3231_print_alarm_1() {
    if (alarm.a1m1)
        uart_send_string("A1M1: ON");
    else
        uart_send_string("A1M1: OFF");
    uart_send_string(" A1 second: ");

    uart_send_hex_uint8(alarm.second_a1);
    uart_send_char('\n');

    if (alarm.a1m2)
        uart_send_string("A1M2: ON");
    else
        uart_send_string("A1M2: OFF");
    uart_send_string(" A1 minute: ");
    uart_send_hex_uint8(alarm.minute_a1);
    uart_send_char('\n');

    if (alarm.a1m3)
        uart_send_string("A1M3: ON");
    else
        uart_send_string("A1M3: OFF");
    uart_send_string(" A1 hour: ");
    uart_send_hex_uint8(alarm.hour_a1);
    uart_send_char('\n');

    if (alarm.a1m4)
        uart_send_string("A1M4: ON");
    else
        uart_send_string("A1M4: OFF");
    if (alarm.dydt_a1)
        uart_send_string(" DY/DT: ON, day is: ");
    else
        uart_send_string(" DY/DT: OFF, date is: ");

    uart_send_hex_uint8(alarm.day_date_a1);
    uart_send_char('\n');
}

void ds3231_print_alarm_2() {
    if (alarm.a2m2)
        uart_send_string("A2M2: ON");
    else
        uart_send_string("A2M2: OFF");
    uart_send_string(" A2 minute: ");
	uart_send_hex_uint8(alarm.minute_a2);
    uart_send_char('\n');

    if (alarm.a2m3)
        uart_send_string("A2M3: ON");
    else
        uart_send_string("A2M3: OFF");
    uart_send_string(" A2 hour: ");
	uart_send_hex_uint8(alarm.hour_a2);
    uart_send_char('\n');

    if (alarm.a2m4)
        uart_send_string("A2M4: ON");
    else
        uart_send_string("A2M4: OFF");
    if (alarm.dydt_a2)
        uart_send_string(" DY/DT: ON, day is: ");
    else
        uart_send_string(" DY/DT: OFF, date is: ");

	uart_send_hex_uint8(alarm.day_date_a2);
    uart_send_char('\n');
}

void ds3231_print_calendar(){
    uart_send_char('\n');
    // print date
/*    uart_send_string("year: "); 
    uart_send_uint8(ds3231_get(YEAR));
    uart_send_string(" month: ");
    uart_send_uint8(ds3231_get(MONTH));
    uart_send_string(" date: "); 
    uart_send_uint8(ds3231_get(DATE));
    uart_send_string(" day: ");
    uart_send_uint8(ds3231_get(DAY));
    uart_send_char('\n');
*/
    // print time
    uart_send_uint8(ds3231_get(HOUR));
    uart_send_char(':');
    uart_send_uint8(ds3231_get(MINUTE));
    uart_send_char(':');
    uart_send_uint8(ds3231_get(SECOND));
    uart_send_string(" temp= ");
    uart_send_uint8(ds3231_get_temperature());
    uart_send_char('.');
    uart_send_char(ds3231_get_temperature_fraction());
    uart_send_string(" ctl= ");
    uart_send_hex_uint8(ds3231_get_control());
    uart_send_string(" sts= ");
    uart_send_hex_uint8(ds3231_get_status());
    uart_send_char('\n');
}
