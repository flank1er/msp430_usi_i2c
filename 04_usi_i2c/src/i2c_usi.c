// based on https://github.com/samerpav/MMA8452_MSP430/blob/master/i2c_usi_mst.c
#include "msp430g2452.h"
#include "sys/types.h"
#include "i2c_usi.h"

// ack = !LAST
#define LAST 1
#define NOLAST 0
//// status code ////////////
#define SUCCESS 1
#define FAILURE 0
////////////////

static uint8_t i2c_count;

#define SET_SDA_AS_OUTPUT()             (USICTL0 |= USIOE)
#define SET_SDA_AS_INPUT()              (USICTL0 &= ~USIOE)

#define FORCING_SDA_HIGH()           \
        {                            \
          USISRL = 0xFF;             \
          USICTL0 |= USIGE;          \
          USICTL0 &= ~(USIGE+USIOE); \
        }

#define FORCING_SDA_LOW()         \
        {                         \
          USISRL = 0x00;          \
          USICTL0 |= USIGE+USIOE; \
          USICTL0 &= ~USIGE;      \
        }

// USI I2C ISR function //////////////////////////////
#pragma vector = USI_VECTOR
__interrupt void USI_ISR (void)
{
    USICTL1 &= ~USIIFG;
  	__bic_SR_register_on_exit(LPM0_bits + GIE);
}

/////////////// I2C //////////////////////////////////
void init_i2c ()
{
	P1DIR = 0xFF;
	sdaDIR &= ~SDA; sclDIR &= ~SCL;
	sdaOUT &= ~SDA; sclOUT &= ~SCL;
	sdaREN |= SDA;  sclREN |= SCL; // это необязательно, если на шине уже есть подтягивающие резисторы
	P1SEL = BIT6 + BIT7;

	USICTL0 = USIPE6 + USIPE7 + USIMST + USISWRST;  // Port & USI mode setup
	USICTL1 = USII2C + USISTTIE + USIIE;            // Enable I2C mode & USI interrupt
	USICKCTL = USIDIV_7 + USISSEL_2 + USICKPL;      // USI clks: SCL = SMCLK/128
	//USICKCTL = USIDIV_6 + USISSEL_2 + USICKPL;      // USI clks: SCL = SMCLK/64
	USICNT |= USIIFGCC ;                            // Disable automatic clear control
	USICTL0 &= ~USISWRST;                           // Enable USI
	USICTL1 &= ~USIIFG;               				// Clear pending flag
}

uint8_t start_i2c() {
    USISRL = 0x00;
    USICTL0 |= USIGE+USIOE;
    USICTL0 &= ~USIGE;

return SUCCESS;
}

uint8_t stop_i2c() 
{
  	USICTL0 |= USIOE;
  	USISRL = 0x00;
  	USICNT = 1;

  	// wait for USIIFG is set
    __bis_SR_register(LPM0_bits + GIE);

  	FORCING_SDA_HIGH();
	return 0; // always success
}

uint8_t send_i2c(uint8_t byte)
{
	// send address and R/W bit
	SET_SDA_AS_OUTPUT();
  	USISRL = byte;
  	USICNT = (USICNT & 0xE0) + 8;

  	// wait until USIIFG is set
    __bis_SR_register(LPM0_bits + GIE);

  	// check NACK/ACK
  	SET_SDA_AS_INPUT();
  	USICNT = (USICNT & 0xE0) + 1;

  	// wait for USIIFG is set
    __bis_SR_register(LPM0_bits + GIE);

	// NACK received returns FALSE
	return (USISRL & 0x01) ? SUCCESS : FAILURE;
}

uint8_t read_i2c(int last)
{
	SET_SDA_AS_INPUT();
	USICNT = (USICNT & 0xE0) + 8;

	// wait for USIIFG is set
    __bis_SR_register(LPM0_bits + GIE);

	uint8_t ret=USISRL;

	SET_SDA_AS_OUTPUT();
	USISRL=(last) ? 0xff : 0x00;

	USICNT = (USICNT & 0xE0) + 1;

	// wait until USIIFG is set
    __bis_SR_register(LPM0_bits + GIE);

	// set SDA as input
	SET_SDA_AS_INPUT();

  return ret;
}

uint8_t i2c_begin(uint8_t adr) {
    int attempts;
    uint8_t ack=0xff;
    attempts=start_i2c();

    if (attempts)
        ack=send_i2c(adr);
    // 0 - Error, 1 - Success
    return (!attempts || ack ) ? 0: 1;
}

uint8_t i2c_request_from(uint8_t ard, uint8_t cnt) {
    i2c_count=cnt;
    return (i2c_begin((ard<<1)|1)) ? 1 : 0;
}

uint8_t i2c_read() {
    uint8_t ret=0;
    if (i2c_count)
    {
        i2c_count--;
        if  (!i2c_count) {
            ret=read_i2c(LAST);
            stop_i2c();
        } else
            ret=read_i2c(NOLAST);
    }
    return ret;
}
