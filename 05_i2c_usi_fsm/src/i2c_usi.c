#include "msp430g2452.h"
#include "sys/types.h"
#include "i2c_usi.h"

//// status code ////////////
#define SUCCESS 1
#define FAILURE 0

//////////////////////////
static uint8_t i2c_count;

volatile uint8_t state;
volatile uint8_t AckResult;
volatile uint8_t slv_adr;
volatile uint8_t ls;
/////////////// I2C //////////////////////////////////
void init_i2c ()
{
	sdaDIR &= ~SDA; sclDIR &= ~SCL;
	sdaOUT &= ~SDA; sclOUT &= ~SCL;
	sdaREN |= SDA;  sclREN |= SCL;
	P1SEL = BIT6 + BIT7;

	USICTL0 = USIPE6 + USIPE7 + USIMST + USISWRST;  // Port & USI mode setup
	USICTL1 = USII2C +  USIIE;            			// Enable I2C mode & USI interrupt
	USICKCTL = USIDIV_7 + USISSEL_2 + USICKPL;      // USI clks: SCL = SMCLK/128
	//USICKCTL = USIDIV_6 + USISSEL_2 + USICKPL;    // USI clks: SCL = SMCLK/64
	USICNT |= USIIFGCC ;                            // Disable automatic clear control
	USICTL0 &= ~USISWRST;                           // Enable USI
	USICTL1 &= ~USIIFG;               				// Clear pending flag
}


uint8_t stop_i2c()
{
	fsm_i2c(0x0, STOP, NOLAST);
	return 0; // always success
}

uint8_t send_i2c(uint8_t value){
	return (fsm_i2c(value, SEND, LAST)) ? 0 : 1;
}

uint8_t read_i2c(int last){
	return fsm_i2c(0x0, GET, last);
}

uint8_t i2c_begin(uint8_t adr) {
	return fsm_i2c(adr, START, NOLAST);
}

uint8_t i2c_request_from(uint8_t ard, uint8_t cnt) {
    i2c_count=cnt;
    return (i2c_begin((ard<<1)|1)) ? 1 : 0;
}

uint8_t i2c_read() {
    uint8_t ret=0;
    if (i2c_count)
    {
        i2c_count--;
        if  (!i2c_count) {
            ret=read_i2c(LAST);
            stop_i2c();
        } else
            ret=read_i2c(NOLAST);
    }
    return ret;
}


uint8_t fsm_i2c(uint8_t adr, uint8_t st, uint8_t last) {
    slv_adr=adr;
    ls=last;

    state=st;
    do {
        if (state == START || state == SEND || state == STOP || state == GET) {
            __disable_interrupt();
            USICTL1|=USIIFG;
        }
        __bis_SR_register(LPM0_bits + GIE);
    } while (state != 10);
    return AckResult;
}

// USI I2C ISR function
#pragma vector = USI_VECTOR
__interrupt void USI_ISR (void)
{
	switch (state) {
	case 0: 
        USISRL = 0x00; // MSB=0
        USICTL0 |= USIGE+USIOE; // SDA as OUTPUT, turn off LATCH
        USICTL0 &= ~USIGE; // turn on LATCH
		state=1;
		break;
	case 1: 
		// start
		// send address
		USICTL0|=USIOE; //sda as OUTPUT
		USISRL = slv_adr; //  send data
		USICNT=8; //bit counter
		state=2;
		break;
    case 2: 
		USICTL0 &= ~USIOE; //SET_SDA_AS_INPUT();
    	USICNT = 1; // bit counter
		state=3;
        break;
    case 3: // get ACK
		if (USISRL & 0x01)
		{
			AckResult=FAILURE;
			state=4; // prestop
		} else {
			AckResult=SUCCESS;
			state=10; // exit
		}
        break;
	case 4: // prestop
	    USICTL0 |= USIOE; // sda as OUTPUT
    	USISRL = 0x00;
    	USICNT = 1;
		state=5; // goto STOP
		break;
	case 5:
    	USISRL = 0xFF;
    	USICTL0 |= USIGE;		   // Transparent latch enabled
    	USICTL0 &= ~(USIGE+USIOE); // Latch/SDA output disabled
		state=10;
		break;
    case 6:
        USICTL0 &= ~USIOE; //SET_SDA_AS_INPUT();
        USICNT = 8;
		state=7;
        break;
	case 7:
        AckResult = USISRL; //get data
		USICTL0|=USIOE; // sda as OUTPUT
    	USISRL=(ls) ? 0xff : 0x00;
		USICNT=1;
		state=8;
		break;
	case 8:
		USICTL0 &= ~USIOE; //sda as input
		state=(ls) ? 4 : 10;
		break;
	default:
		break;
	}

    USICTL1 &= ~USIIFG;
	__bic_SR_register_on_exit(LPM0_bits + GIE);
}
