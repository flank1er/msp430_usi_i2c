#include "msp430g2452.h"
#include "sys/types.h"
#include "timer_a.h"

volatile uint16_t count;
// for software uart
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0(void)
{
    __bic_SR_register_on_exit(LPM0_bits);
}

// for wait_ms()
#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer0_OVF(void)
{
    switch(TA0IV)
    {
        case TA0IV_TACCR1: break;             // CCR1 not used
        case TA0IV_TACCR2: break;             // CCR2 not used
        case TA0IV_TAIFG: 
            if (count)  
                count--;    // overflow
            else
                __bic_SR_register_on_exit(LPM0_bits);
            break;
    }
}

void wait_ms(uint16_t ms) {
    count=ms;
    TACCR0=1000; // delay
    TACTL |= TAIE + MC_1; // enable interrupt
    __bis_SR_register(LPM0_bits + GIE);
	TACTL &= ~(TAIE + MC_1);
}

