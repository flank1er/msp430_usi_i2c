#ifndef __I2C_USI_H__
#define __I2C_USI_H__

#define SCL     BIT6
#define SDA     BIT7
#define sclOUT  P1OUT
#define sdaOUT  P1OUT
#define sclDIR  P1DIR
#define sdaDIR  P1DIR
#define sclREN  P1REN
#define sdaREN  P1REN
#define sdaIN   P1IN
#define sclIN   P1IN

#define NOLAST 0x0
#define LAST 0x1

#define START   0x0
#define SEND    0x1
#define STOP    0x4
#define GET     0x6


void init_i2c();
uint8_t stop_i2c();
uint8_t send_i2c(uint8_t value);
uint8_t read_i2c(int last);
uint8_t i2c_begin(uint8_t adr);
uint8_t i2c_request_from(uint8_t ard, uint8_t cnt);
uint8_t i2c_read();

uint8_t fsm_i2c(uint8_t adr, uint8_t st, uint8_t last);
#endif
